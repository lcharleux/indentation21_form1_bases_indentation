# Formation Bases de l'Indentation (Indentation 2021)

This training aims to introduce you to the basics of indentation testing. From a pedagogical point of view, the choice was made to emphasize the practical application of all the notions presented. This tutorial goes from basic data processing in Python to the interpretation of a batch of nanoindentation tests.
